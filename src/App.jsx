import React, { useState } from 'react';
import './App.css';
import Homes from "./pages/Home";
import Header from "./components/Header";
import Footer from './components/Footer';
import Erreur from './components/Erreur';
import StartScreen from './components/JeuMaths/GameScreen';
import GameScreen from './components/JeuMaths/StartScreen';
import CompleterPhrases from './components/CompleterPhrases/CompleterPhrases';
import ColorBox from './components/BoxCouleur/colorBox';
import Accueil from './pages/Accueil';

import ImageChat from './resources/ImageChat';
import Imchat from './resources/Imchat';
import Contentoggle from './components/Toggle/Contentoggle';




function App() {
    const [studentName, setStudentName] = useState('');
    const [isGameStarted, setIsGameStarted] = useState(false);

    const startGame = (name) => {
        setStudentName(name);
        setIsGameStarted(true);
    };

    return (
        <div className="App">
            <Header />


            <main>
                {isGameStarted ? (
                    <GameScreen studentName={studentName} />
                ) : (
                    <StartScreen startGame={startGame} />
                )}
                <Homes />

                <Accueil></Accueil>
                <ImageChat></ImageChat>
                
                <Contentoggle titre="titre">
                    <Imchat></Imchat>
                    <ColorBox heading="Attention">
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illo id dicta, esse recusandae obcaecati explicabo sequi necessitatibus
                            accusantium vel a facere voluptatibus amet ipsa laboriosam tempora nihil aspernatur numquam! Fuga.</p>
                    </ColorBox>

                </Contentoggle>


                <p>
                    Lorem <Erreur>ipsum dolor sit amet consectetur adipisicing elit.
                        Quam cumque nam reprehenderit nobis veniam dolorem maxime</Erreur>
                    ducimus! Aut adipisci eius itaque magnam pariatur. Expedita impedit
                    eveniet pariatur repellendus. Nisi, cumque.
                </p>

                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro assumenda recusandae molestias.
                    <ColorBox heading="Attention"> Id dignissimos,
                        ratione quas, harum alias dolor natus,
                        modi ex eaque distinctio excepturi earum? Fuga
                        labore ipsum nesciunt!</ColorBox></p>
                <CompleterPhrases></CompleterPhrases>
            </main>
            <Footer />
        </div>
    );
}

export default App;
