import React, { useState } from 'react';
import "./CompleterPhrases.css"

const CompleterPhrases = () => {
    const initialAnswers = {
        phrase1_1: '',
        phrase1_2: '',
        phrase2_1: '',
        phrase2_2: '',
        phrase3_1: '',
        phrase3_2: '',
        phrase4_1: '',
        phrase4_2: '',
        phrase5_1: '',
        phrase5_2: '',
        phrase5_3: '',
        phrase6_1: '',
        phrase6_2: '',
        phrase6_3: '',
        phrase7_1: '',
        phrase7_2: '',
        phrase7_3: '',
        phrase8_1: '',
        phrase8_2: '',
        phrase8_3: ''
    };

    const correctAnswers = {
        phrase1_1: 'a',
        phrase1_2: 'à',
        phrase2_1: 'à',
        phrase2_2: 'a',
        phrase3_1: 'à',
        phrase3_2: 'a',
        phrase4_1: 'a',
        phrase4_2: 'a',
        phrase5_1: 'a',
        phrase5_2: 'à',
        phrase5_3: 'a',
        phrase6_1: 'À',
        phrase6_2: 'à',
        phrase6_3: 'a',
        phrase7_1: 'à',
        phrase7_2: 'à',
        phrase7_3: 'a',
        phrase8_1: 'a',
        phrase8_2: 'à',
        phrase8_3: 'à'
    };

    const [userAnswers, setUserAnswers] = useState(initialAnswers);
    const [resultColors, setResultColors] = useState(initialAnswers);

    const handleChange = (event) => {
        const { id, value } = event.target;
        setUserAnswers((prevAnswers) => ({
            ...prevAnswers,
            [id]: value
        }));
    };

    const checkAnswers = () => {
        let correct = true;
        let newResultColors = { ...initialAnswers };

        for (const id in correctAnswers) {
            if (userAnswers[id].trim() !== correctAnswers[id]) {
                correct = false;
                newResultColors[id] = 'red';
            } else {
                newResultColors[id] = 'green';
            }
        }

        setResultColors(newResultColors);

        if (correct) {
            alert("Toutes les réponses sont correctes !");
        } else {
            alert("Certaines réponses sont incorrectes. Veuillez vérifier les champs en rouge.");
        }
    };

    return (
        <form id="phrasesForm">
            <p>Elle <input type="text" id="phrase1_1" size="1" value={userAnswers.phrase1_1} onChange={handleChange} style={{ backgroundColor: resultColors.phrase1_1 }} /> décidé de reprendre ses cours de piano <input type="text" id="phrase1_2" size="1" value={userAnswers.phrase1_2} onChange={handleChange} style={{ backgroundColor: resultColors.phrase1_2 }} /> partir de la semaine prochaine.</p>
            <p>Je n’ai plus envie d’aller <input type="text" id="phrase2_1" size="1" value={userAnswers.phrase2_1} onChange={handleChange} style={{ backgroundColor: resultColors.phrase2_1 }} /> la piscine, il y <input type="text" id="phrase2_2" size="1" value={userAnswers.phrase2_2} onChange={handleChange} style={{ backgroundColor: resultColors.phrase2_2 }} /> trop de monde.</p>
            <p>Tu es partie <input type="text" id="phrase3_1" size="1" value={userAnswers.phrase3_1} onChange={handleChange} style={{ backgroundColor: resultColors.phrase3_1 }} /> cause de la pluie ou parce qu’il <input type="text" id="phrase3_2" size="1" value={userAnswers.phrase3_2} onChange={handleChange} style={{ backgroundColor: resultColors.phrase3_2 }} /> fait trop chaud ?</p>
            <p>Il <input type="text" id="phrase4_1" size="1" value={userAnswers.phrase4_1} onChange={handleChange} style={{ backgroundColor: resultColors.phrase4_1 }} /> tellement plu que Geneviève <input type="text" id="phrase4_2" size="1" value={userAnswers.phrase4_2} onChange={handleChange} style={{ backgroundColor: resultColors.phrase4_2 }} /> dû s’acheter un parapluie.</p>
            <p>Philippe <input type="text" id="phrase5_1" size="1" value={userAnswers.phrase5_1} onChange={handleChange} style={{ backgroundColor: resultColors.phrase5_1 }} /> dérapé <input type="text" id="phrase5_2" size="1" value={userAnswers.phrase5_2} onChange={handleChange} style={{ backgroundColor: resultColors.phrase5_2 }} /> cause du verglas, maintenant il <input type="text" id="phrase5_3" size="1" value={userAnswers.phrase5_3} onChange={handleChange} style={{ backgroundColor: resultColors.phrase5_3 }} /> peur.</p>
            <p><input type="text" id="phrase6_1" size="1" value={userAnswers.phrase6_1} onChange={handleChange} style={{ backgroundColor: resultColors.phrase6_1 }} /> Champlain, va jusqu’à la sortie du village et tourne <input type="text" id="phrase6_2" size="1" value={userAnswers.phrase6_2} onChange={handleChange} style={{ backgroundColor: resultColors.phrase6_2 }} /> gauche, il y <input type="text" id="phrase6_3" size="1" value={userAnswers.phrase6_3} onChange={handleChange} style={{ backgroundColor: resultColors.phrase6_3 }} /> un panneau.</p>
            <p>Tu as de la route <input type="text" id="phrase7_1" size="1" value={userAnswers.phrase7_1} onChange={handleChange} style={{ backgroundColor: resultColors.phrase7_1 }} /> faire si tu veux t’acheter <input type="text" id="phrase7_2" size="1" value={userAnswers.phrase7_2} onChange={handleChange} style={{ backgroundColor: resultColors.phrase7_2 }} /> manger, ici il n’y <input type="text" id="phrase7_3" size="1" value={userAnswers.phrase7_3} onChange={handleChange} style={{ backgroundColor: resultColors.phrase7_3 }} /> ni boucherie ni boulangerie.</p>
            <p>Il n’y <input type="text" id="phrase8_1" size="1" value={userAnswers.phrase8_1} onChange={handleChange} style={{ backgroundColor: resultColors.phrase8_1 }} /> ni chauffage ni télé, on gèle et on s’ennuie, alors nous allons tous les jours <input type="text" id="phrase8_2" size="1" value={userAnswers.phrase8_2} onChange={handleChange} style={{ backgroundColor: resultColors.phrase8_2 }} /> pied rendre visite <input type="text" id="phrase8_3" size="1" value={userAnswers.phrase8_3} onChange={handleChange} style={{ backgroundColor: resultColors.phrase8_3 }} /> nos cousins.</p>
            <button type="button" onClick={checkAnswers}>Vérifier les réponses</button>
        </form>
    );
};

export default CompleterPhrases;
