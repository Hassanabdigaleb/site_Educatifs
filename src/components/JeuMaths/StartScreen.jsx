import React, { useState } from 'react';

function StartScreen({ startGame }) {
    const [name, setName] = useState('');

    const handleStart = () => {
        if (name.trim()) {
            startGame(name);
        } else {
            alert("Veuillez entrer votre nom.");
        }
    };

    return (
        <div className="container">
            <h1>Calcul de la Somme</h1>
            <p>Veuillez entrer votre nom :</p>
            <input
                type="text"
                value={name}
                onChange={(e) => setName(e.target.value)}
                placeholder="Votre nom"
            />
            <button onClick={handleStart}>Démarrer le jeu</button>
        </div>
    );
}

export default StartScreen;
