import React from 'react';
import styles from './Header.module.css';

const Header = () => {
    return (
        <header className={styles.header}>
            <div className={styles.container}>
                <h1 className={styles.title}>Site Éducatif Canadien</h1>
                <nav className={styles.nav}>
                    <ul className={styles.mainNav}>
                        <li className={styles.navItem}>
                            <a href="/accueil" className={styles.linke}>Accueil</a>
                        </li>
                        <li className={styles.navItem}>
                            <a href="/niveau" className={styles.linke}>Niveaux scolaires</a>
                            <ul className={styles.subMenu}>
                                <li className={styles.subNavItem}>
                                    <a href="/primaire" className={styles.linke}>Primaire</a>
                                    <ul className={styles.subSubMenu}>
                                        <li><a href="/Maternelle" className={styles.linke}>Maternelle</a></li>
                                        <li><a href="/Grande" className={styles.linke}>Grande section (G1-G2)</a></li>
                                        <li><a href="/cours" className={styles.linke}>Cours préparatoire (CP-CP)</a></li>
                                        <li><a href="/3e" className={styles.linke}>3e année</a></li>
                                        <li><a href="/4e" className={styles.linke}>4e année</a></li>
                                        <li><a href="/5e" className={styles.linke}>5e année</a></li>
                                        <li><a href="/6e" className={styles.linke}>6e année</a></li>
                                    </ul>
                                </li>
                                <li className={styles.subNavItem}>
                                    <a href="/secondaire" className={styles.linke}>Secondaire</a>
                                    <ul className={styles.subSubMenu}>
                                        <li><a href="/7e" className={styles.linke}>7e année</a></li>
                                        <li><a href="/8e" className={styles.linke}>8e année</a></li>
                                        <li><a href="/9e" className={styles.linke}>9e année</a></li>
                                        <li><a href="/10e" className={styles.link}>10e année</a></li>
                                        <li><a href="/11e" className={styles.linke}>11e année</a></li>
                                        <li><a href="12e" className={styles.linke}>12e année</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li className={styles.navItem}>
                            <a href="/ressources" className={styles.linke}>Ressources</a>
                            <ul className={styles.subMenu}>
                                <li><a href="/bibliotheque" className={styles.linke}>Bibliothèque</a></li>
                                <li><a href="/tutoriels" className={styles.linke}>Tutoriels</a></li>
                                <li><a href="/article" className={styles.linke}>Articles</a></li>
                            </ul>
                        </li>
                        <li className={styles.navItem}>
                            <a href="/apropos" className={styles.linke}>À propos</a>
                        </li>
                        <li className={styles.navItem}>
                            <a href="/contact" className={styles.linke}>Contact</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </header>
    );
};

export default Header;
