import React from 'react';
import st from "./Imgchat.module.css";

function Imchat() {
    let tab = [
        { src: "https://th.bing.com/th?id=OIP.JQ1yR5m0DAk46vwvq-E_kwHaLL&w=203&h=307&c=8&rs=1&qlt=90&o=6&dpr=1.3&pid=3.1&rm=2", alt: "Chat mignon avec un chapeau" },
        { src: "https://th.bing.com/th?id=OIP.sRi_xxyUGW6EOva5QIurMAAAAA&w=208&h=299&c=8&rs=1&qlt=90&o=6&dpr=1.3&pid=3.1&rm=2", alt: "Chat jouant avec une pelote de laine" },
        { src: "https://th.bing.com/th?id=OIP.PruNhkOIqtt1AkEr2GkaagHaEK&w=333&h=187&c=8&rs=1&qlt=90&o=6&dpr=1.3&pid=3.1&rm=2", alt: "Chat endormi sur un canapé" },
        { src: "https://th.bing.com/th?id=OIP.sDZBXbZ41cu6tXK2Q9GbywHaN2&w=182&h=341&c=8&rs=1&qlt=90&o=6&dpr=1.3&pid=3.1&rm=2", alt: "Chaton curieux regardant par la fenêtre" }
    ];

    const googleFormLink = "https://forms.gle/yk1oiXMKYTvrGGLR6"; // Remplace par ton lien Google Form

    return (
        <div>
            <div className={st.t}>
                {tab.map((image, index) => (
                    <div key={index} className={st.imgContainer}>
                        <img src={image.src} alt={image.alt} />
                        <p>{image.alt}</p>
                        <a href={image.src} download={`image${index + 1}.jpg`}>
                            <button>Télécharger</button>
                        </a>
                    </div>
                ))}
            </div>
            <div className={st.formContainer}>
                <p>Veuillez remplir ce <a href={googleFormLink} target="_blank" rel="noopener noreferrer">formulaire Google</a>.</p>
            </div>
        </div>
    );
}

export default Imchat;
