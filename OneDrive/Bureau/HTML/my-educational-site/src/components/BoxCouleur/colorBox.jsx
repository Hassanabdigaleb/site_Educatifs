import React from 'react'
import styles from "./colorBox.module.css"

export default function ColorBox(props) {
  return (
    <div className={styles.box}>
        <p className={styles.heading}>{props.heading}</p>
        <div>{props.children}</div>
      
    </div>
  )
}

