import React from 'react';
import styles from "./Footer.module.css";

const Footer = () => {
  return (
    <footer className={styles.footer}>
      <p>&copy; {new Date().getFullYear()} - Votre site pedagogique</p>
      
    </footer>
  );
};

export default Footer;