import React, { useState, useEffect } from 'react';
import Chart from './Chart'; // Assurez-vous que ce chemin est correct


function GameScreen({ studentName }) {
    const [num1, setNum1] = useState(0);
    const [num2, setNum2] = useState(0);
    const [sumInput, setSumInput] = useState('');
    const [result, setResult] = useState('');
    const [totalQuestions, setTotalQuestions] = useState(0);
    const [correctAnswers, setCorrectAnswers] = useState(0);
    const [incorrectAnswers, setIncorrectAnswers] = useState(0);

    useEffect(() => {
        generateNumbers();
    }, []);

    const generateNumbers = () => {
        setNum1(Math.floor(Math.random() * 100));
        setNum2(Math.floor(Math.random() * 100));
        setResult('');
        setSumInput('');
    };

    const verifySum = () => {
        if (sumInput.trim() === '') {
            setResult("Veuillez entrer une réponse.");
            return;
        }

        setTotalQuestions(totalQuestions + 1);
        if (parseInt(sumInput) === num1 + num2) {
            setCorrectAnswers(correctAnswers + 1);
            setResult("Très bien!");
        } else {
            setIncorrectAnswers(incorrectAnswers + 1);
            setResult("Incorrect. Réessayez.");
        }

        generateNumbers();
    };

    const replay = () => {
        setTotalQuestions(0);
        setCorrectAnswers(0);
        setIncorrectAnswers(0);
        generateNumbers();
    };

    return (
        <div id="content" style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'center', alignItems: 'flex-start' }}>
            <div className="game-container container">
                <p>{studentName}, quelle est la somme de {num1} et {num2} ?</p>
                <input
                    type="text"
                    value={sumInput}
                    onChange={(e) => setSumInput(e.target.value)}
                    placeholder="Votre réponse"
                />
                <button onClick={verifySum}>Soumettre</button>
                <button onClick={replay}>Rejouer</button>
                <div id="result">{result}</div>
            </div>
            <Chart totalQuestions={totalQuestions} correctAnswers={correctAnswers} incorrectAnswers={incorrectAnswers} />
        </div>
    );
}

export default GameScreen;
