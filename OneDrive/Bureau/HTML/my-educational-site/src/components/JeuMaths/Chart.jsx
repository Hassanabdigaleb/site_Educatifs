import React from 'react';
import { Bar } from 'react-chartjs-2';
import "../JeuMaths/jeu.css"
import 'chart.js/auto'; // Cette ligne enregistre automatiquement tous les éléments de Chart.js

function Chart({ totalQuestions, correctAnswers, incorrectAnswers }) {
    const data = {
        labels: ['Total Questions', 'Correctes', 'Incorrectes'],
        datasets: [{
            label: 'Bilan des Résultats',
            data: [totalQuestions, correctAnswers, incorrectAnswers],
            backgroundColor: [
                'rgba(54, 162, 235, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(255, 99, 132, 0.2)'
            ],
            borderColor: [
                'rgba(54, 162, 235, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(255, 99, 132, 1)'
            ],
            borderWidth: 1
        }]
    };

    const options = {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    };

    return (
        <div className="chart-container container">
            <Bar data={data} options={options} />
        </div>
    );
}

export default Chart;
