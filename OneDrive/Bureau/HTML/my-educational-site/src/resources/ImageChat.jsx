import React from 'react';
import st from "./Imgchat.module.css";

function ImageChat() {
    let tab = [
        { name: "Alice", age: 22 },
        { name: "Bob", age: 23 },
        { name: "Charlie", age: 24 },
        { name: "Diana", age: 25 }
    ];

    return (
        <div className={st.t}>
            {tab.map((person, index) => (
                <div key={index} className={st.itemContainer}>
                    <h3>{person.name}</h3>
                    <p>Âge: {person.age}</p>
                </div>
            ))}
        </div>
    );
}

export default ImageChat;
