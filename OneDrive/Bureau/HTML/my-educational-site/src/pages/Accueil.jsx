import { useState } from 'react';


export default function Accueil() {
  const [compter, setCompter] = useState(0);

  const inc = () => {
    setCompter(compter + 1);
  };

  const dec = () => {
    setCompter(compter - 1);
  };

  return (
    <div>
    
      <button onClick={inc}>Increment</button>
      <span>{compter}</span>
      <button onClick={dec}>Decrement</button>
    </div>
  );
}


