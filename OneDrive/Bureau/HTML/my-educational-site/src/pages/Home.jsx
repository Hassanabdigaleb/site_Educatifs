import React from 'react';
import Erreur from '../components/Erreur';


export default function Home  () {
  return (
    <div>
      
      <h1>Welcome to My Educational Site!</h1>
      <p>This is a sample educational website.</p>
      <p>Lorem ipsum dolor sit, amet <Erreur> consectetur  adipisicing elit</Erreur>. Minus corrupti 
        recusandae aliquid mollitia laboriosam. Asperiores dolorem ipsum perferendis debitis provident, 
        officia sequi, dolorum et aliquid esse similique. Rem, hic dicta.</p>
      

    </div>
  );
};